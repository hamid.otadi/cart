#!/bin/sh
#
# See https://blog.ch.atosconsulting.com/configure-spring-boot-docker-secrets/

#if [ -d /run/secrets/ ]; then
#    for filename in /run/secrets/*; do
#        export ${filename##*/}=`cat $filename`
#    done
#fi

exec java ${JAVA_OPTS} -Djava.security.egd=file:/dev/./urandom -jar "app.jar" "$@"
