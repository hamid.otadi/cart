package shop.velox.cart.api.controller.impl;

import java.util.List;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;
import shop.velox.cart.api.controller.CartController;
import shop.velox.cart.api.dto.CartDto;
import shop.velox.cart.model.CartEntity;
import shop.velox.cart.service.CartService;
import shop.velox.commons.converter.Converter;


@RestController
public class CartControllerImpl implements CartController {

  private static final Logger LOG = LoggerFactory.getLogger(CartControllerImpl.class);

  private final CartService cartService;

  private final Converter<CartEntity, CartDto> cartConverter;

  public CartControllerImpl(@Autowired final CartService cartService,
      @Autowired final Converter<CartEntity, CartDto> cartConverter) {
    this.cartService = cartService;
    this.cartConverter = cartConverter;
  }

  @Override
  public Mono<CartDto> createCart(final CartDto cartDto, final String currentUser) {
    LOG.info("createCart");
    return Mono.fromSupplier(() -> getCartConverter().convertEntityToDto(
        getCartService().createCart(getCartConverter().convertDtoToEntity(cartDto), currentUser)));
  }

  @Override
  public Mono<ResponseEntity<CartDto>> getCart(final String id) {
    return Mono
        .fromSupplier(() -> getCartService().getCart(id)
            .map(cartEntity -> new ResponseEntity<>(
                getCartConverter().convertEntityToDto(cartEntity), null, HttpStatus.OK))
            .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND)));
  }

  @Override
  public Mono<ResponseEntity<Void>> removeCart(final String cartId) {
    return Mono.fromSupplier(
        () -> cartService.deleteCart(cartId) ? new ResponseEntity<>(HttpStatus.NO_CONTENT)
            : new ResponseEntity<>(HttpStatus.NOT_FOUND));
  }

  @Override
  public Page<CartDto> getCarts(final Pageable pageable) {
    Page<CartEntity> cartEntities = cartService.findAll(pageable);
    List<CartDto> cartDtos = cartEntities.getContent().stream()
        .map(getCartConverter()::convertEntityToDto)
        .collect(Collectors.toList());
    return new PageImpl<>(cartDtos, pageable, cartEntities.getTotalElements());
  }

  @Override
  public Mono<ResponseEntity<CartDto>> updateCart(final String cartId, final CartDto cart,
      final String customerId) {
    return Mono.fromSupplier(() -> cartService.updateCart(cartId, cart, customerId));
  }

  protected CartService getCartService() {
    return cartService;
  }

  protected Converter<CartEntity, CartDto> getCartConverter() {
    return cartConverter;
  }
}