package shop.velox.cart.api.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import javax.validation.Valid;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import reactor.core.publisher.Mono;
import shop.velox.cart.api.dto.CartDto;


@Tag(name = "Cart", description = "the Cart API")
@RequestMapping("/carts")
public interface CartController {

  @Operation(summary = "create new Cart", description = "")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "201", description = "Cart created", content = @Content(schema = @Schema(implementation = CartDto.class)))
  })
  @PostMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
  @ResponseStatus(HttpStatus.CREATED)
  Mono<CartDto> createCart(
      @Parameter(description = "The cart to create.") @Valid @RequestBody final CartDto cart,
      @AuthenticationPrincipal(expression = "T(shop.velox.commons.security.utils.AuthUtils).principalToCustomerIdMapper.apply(#this)") String customerId);

  @Operation(summary = "Find Cart by code", description = "")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "successful operation", content = @Content(schema = @Schema(implementation = CartDto.class))),
      @ApiResponse(responseCode = "404", description = "cart not found", content = @Content(schema = @Schema()))
  })
  @GetMapping(value = "/{id:.*}", produces = MediaType.APPLICATION_JSON_VALUE)
  Mono<ResponseEntity<CartDto>> getCart(
      @Parameter(description = "Id of the Cart. Cannot be empty.", required = true) @PathVariable("id") final String id);

  @Operation(summary = "Delete Cart by code", description = "")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "204", description = "successful operation"),
      @ApiResponse(responseCode = "404", description = "cart not found")
  })
  @DeleteMapping(value = "/{id:.*}")
  Mono<ResponseEntity<Void>> removeCart(
      @Parameter(description = "Id of the Cart. Cannot be empty.", required = true) @PathVariable("id") final String id);

  @Operation(summary = "gets all carts. Paginated.", description = "")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "successful operation", content = @Content(schema = @Schema(implementation = CartDto.class)))
  })
  @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
  Page<CartDto> getCarts(Pageable pageable);

  @Operation(summary = "Update a cart. Update currency and assign anonymous cart by code to current customer", description = "")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "successful operation"),
      @ApiResponse(responseCode = "403", description = "non anonymous cart id provided"),
      @ApiResponse(responseCode = "404", description = "cart not found")
  })
  @PatchMapping(value = "/{id:.*}")
  Mono<ResponseEntity<CartDto>> updateCart(
      @Parameter(description = "Id of the Cart. Cannot be empty.", required = true) @PathVariable("id") final String cartId,
      @Parameter(description = "The cart to update.") @RequestBody(required = false) final CartDto cart,
      @AuthenticationPrincipal(expression = "T(shop.velox.commons.security.utils.AuthUtils).principalToCustomerIdMapper.apply(#this)") String customerId);
}

