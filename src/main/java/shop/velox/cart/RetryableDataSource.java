package shop.velox.cart;

import java.sql.Connection;
import java.sql.SQLException;
import javax.sql.DataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.datasource.AbstractDataSource;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;

/**
 * Needed until https://github.com/spring-projects/spring-boot/issues/4779 is fixed
 */
public class RetryableDataSource extends AbstractDataSource {

  private static final Logger LOG = LoggerFactory.getLogger(RetryableDataSource.class);

  private DataSource delegate;

  public RetryableDataSource(final DataSource delegate) {
    this.delegate = delegate;
  }

  @Override
  @Retryable(maxAttempts = 10, backoff = @Backoff(multiplier = 2.3, maxDelay = 30000))
  public Connection getConnection() throws SQLException {
    return delegate.getConnection();
  }

  @Override
  @Retryable(maxAttempts = 10, backoff = @Backoff(multiplier = 2.3, maxDelay = 30000))
  public Connection getConnection(final String username, final String password)
      throws SQLException {
    return delegate.getConnection(username, password);
  }
}
