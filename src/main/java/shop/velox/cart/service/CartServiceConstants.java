package shop.velox.cart.service;

public final class CartServiceConstants {

  public static class Authorities {

    public static final String CART_ADMIN = "Admin_Cart";
  }

  private CartServiceConstants() {
    // private constructor to prevent instantiation
  }
}
