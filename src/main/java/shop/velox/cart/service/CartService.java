package shop.velox.cart.service;

import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import shop.velox.cart.api.dto.CartDto;
import shop.velox.cart.api.dto.ItemDto;
import shop.velox.cart.model.CartEntity;
import shop.velox.cart.model.ItemEntity;

/**
 * Executes CRUD operations on Cart and Item
 */
public interface CartService {

  /**
   * Creates a cart for specified customer and returns it
   *
   * @param cart       the cart with the fields to be updated
   * @param customerId unique customer identifier
   * @return the just created cart
   */
  @PreAuthorize(
      "(isAnonymous() && #customerId == null)"
          + "|| @veloxAuthorizationEvaluator.isCurrentUserId(authentication, #customerId)"
          + "|| @veloxAuthorizationEvaluator.hasGlobalOrCustomAdminAuthority(authentication,"
          + "       T(shop.velox.cart.service.CartServiceConstants.Authorities).CART_ADMIN)"
  )
  CartEntity createCart(CartEntity cart, String customerId);

  /**
   * Gets a cart by its id
   *
   * @param cartId the id of the cart
   * @return the cart wrapped in a {@link Optional}
   */
  @PostAuthorize(
      "(returnObject.isPresent() &&"
          + "   ((isAnonymous() && T(shop.velox.cart.model.CartEntity).anonymousCart.test(returnObject.get()))"
          + "   || (hasPermission(returnObject.get(), 'owner'))))"
          + "|| @veloxAuthorizationEvaluator.hasGlobalOrCustomAdminAuthority(authentication,"
          + "       T(shop.velox.cart.service.CartServiceConstants.Authorities).CART_ADMIN)"
  )
  Optional<CartEntity> getCart(String cartId);

  /**
   * Gets an item by cart id and item id
   *
   * @param cartId the id of the cart
   * @param itemId the id of the item
   * @return the item wrapped in a {@link Optional}
   */
  @PreAuthorize(
      "@cartService.getCart(#cartId).isPresent()"
          + "|| @veloxAuthorizationEvaluator.hasGlobalOrCustomAdminAuthority(authentication,"
          + "       T(shop.velox.cart.service.CartServiceConstants.Authorities).CART_ADMIN)"
  )
  Optional<ItemEntity> getItem(String cartId, String itemId);

  /**
   * Creates an item in the cart with id cartId, with given articleId and quantity
   *
   * @param cartId the id of the cart
   * @param item   contains the information related to the item
   * @return the updated cart
   */
  @PreAuthorize(
      "@cartService.getCart(#cartId).isPresent()"
          + "|| @veloxAuthorizationEvaluator.hasGlobalOrCustomAdminAuthority(authentication,"
          + "       T(shop.velox.cart.service.CartServiceConstants.Authorities).CART_ADMIN)"
  )
  ResponseEntity<CartDto> addItem(String cartId, ItemEntity item);

  /**
   * Updates an item in the cart with id cartId
   *
   * @param cartId the id of the cart
   * @param item   contains the information related to the item
   * @return the updated cart
   */
  @PreAuthorize(
      "@cartService.getCart(#cartId).isPresent()"
          + "|| @veloxAuthorizationEvaluator.hasGlobalOrCustomAdminAuthority(authentication,"
          + "       T(shop.velox.cart.service.CartServiceConstants.Authorities).CART_ADMIN)"
  )
  ResponseEntity<CartDto> updateItem(String cartId, ItemDto item);

  /**
   * Removes an item with given id from cart with given id
   *
   * @param cartId the id of the cart
   * @param itemId the id of the item
   * @return the updated cart
   */
  @PreAuthorize(
      "@cartService.getCart(#cartId).isPresent()"
          + "|| @veloxAuthorizationEvaluator.hasGlobalOrCustomAdminAuthority(authentication,"
          + "       T(shop.velox.cart.service.CartServiceConstants.Authorities).CART_ADMIN)"
  )
  ResponseEntity<CartDto> removeItem(String cartId, String itemId);

  /**
   * Removes a cart with given id
   *
   * @param cartId the id of the cart
   * @return the updated cart
   */
  @PreAuthorize(
      "@cartService.getCart(#cartId).isPresent()"
          + "|| @veloxAuthorizationEvaluator.hasGlobalOrCustomAdminAuthority(authentication,"
          + "       T(shop.velox.cart.service.CartServiceConstants.Authorities).CART_ADMIN)"
  )
  boolean deleteCart(String cartId);

  @PreAuthorize(
      "@veloxAuthorizationEvaluator.hasGlobalOrCustomAdminAuthority(authentication,"
          + "       T(shop.velox.cart.service.CartServiceConstants.Authorities).CART_ADMIN)"
  )
  Page<CartEntity> findAll(Pageable pageable);

  @PreAuthorize(
      "(isAnonymous()"
          + "  && @cartRepository.findById(#cartId)"
          + "    .filter(T(shop.velox.cart.model.CartEntity).anonymousCart)"
          + "    .isPresent()) "
          + "|| (isAuthenticated()"
          + "  && @veloxAuthorizationEvaluator.isCurrentUserId(authentication, #customerId)"
          + "  && ((@cartRepository.findById(#cartId)"
          + "    .filter(T(shop.velox.cart.model.CartEntity).anonymousCart)"
          + "    .isPresent())"
          + "    || @cartRepository.findByIdAndCustomerId(#cartId, #customerId).isPresent()))"
  )
  ResponseEntity<CartDto> updateCart(String cartId, CartDto cartDto, String customerId);
}
