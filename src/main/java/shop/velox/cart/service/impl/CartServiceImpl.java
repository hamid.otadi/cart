package shop.velox.cart.service.impl;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

import java.math.BigDecimal;
import java.net.URI;
import java.util.Optional;
import javax.transaction.Transactional;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import shop.velox.cart.api.controller.ItemController;
import shop.velox.cart.api.dto.CartDto;
import shop.velox.cart.api.dto.ItemDto;
import shop.velox.cart.dao.CartRepository;
import shop.velox.cart.dao.ItemRepository;
import shop.velox.cart.exceptions.NotFoundException;
import shop.velox.cart.model.CartEntity;
import shop.velox.cart.model.ItemEntity;
import shop.velox.cart.service.CartService;
import shop.velox.commons.converter.Converter;

@Service("cartService")
public class CartServiceImpl implements CartService {

  private static final Logger LOG = LoggerFactory.getLogger(CartServiceImpl.class);
  private final CartRepository cartRepository;
  private final ItemRepository itemRepository;
  private final Converter<CartEntity, CartDto> cartConverter;

  CartServiceImpl(@Autowired final CartRepository cartRepository,
      @Autowired final ItemRepository itemRepository,
      final @Autowired Converter<CartEntity, CartDto> cartConverter) {
    this.cartRepository = cartRepository;
    this.itemRepository = itemRepository;
    this.cartConverter = cartConverter;
  }

  @Override
  public CartEntity createCart(CartEntity cart, String customerId) {
    CartEntity newCart = new CartEntity();
    newCart.setCustomerId(customerId);
    newCart.setCurrencyId(cart.getCurrencyId());
    newCart = cartRepository.saveAndFlush(newCart);
    LOG.debug("Created cart {}", newCart);
    return newCart;
  }

  @Override
  public Optional<CartEntity> getCart(final String id) {
    Optional<CartEntity> cartEntity = cartRepository.findById(id);
    if (cartEntity.isEmpty()) {
      throw new NotFoundException("Cart with id(" + id + ") does not exist");
    } else {
      return cartEntity;
    }
  }

  @Override
  public Optional<ItemEntity> getItem(final String cartId, final String itemId) {
    return itemRepository.findItemByCartIdAndId(cartId, itemId);
  }

  @Override
  public ResponseEntity<CartDto> addItem(final String cartId, final ItemEntity item) {
    if (item.getQuantity().compareTo(BigDecimal.ZERO) <= 0) {
      return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
    }

    if (item.getUnitPrice() != null && item.getUnitPrice().compareTo(BigDecimal.ZERO) <= 0) {
      return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
    }

    if (item.getTotalPrice() != null && item.getTotalPrice().compareTo(BigDecimal.ZERO) <= 0) {
      return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
    }

    Optional<CartEntity> cart = getCart(cartId);
    if (cart.isEmpty()) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    Optional<ItemEntity> existingItem = itemRepository
        .findItemByCartIdAndArticleId(cartId, item.getArticleId());
    if (existingItem.isPresent()) {
      HttpHeaders headers = new HttpHeaders();
      String existingItemId = existingItem.get().getId();
      URI existingItemLocation = linkTo(ItemController.class, cartId)
          .slash(existingItemId)
          .toUri();
      headers.setLocation(existingItemLocation);
      return ResponseEntity.status(HttpStatus.CONFLICT)
          .headers(headers)
          .build();
    }

    ItemEntity itemToCreate = new ItemEntity(item.getArticleId(), cart.get(), item.getQuantity(),
        item.getName(), item.getUnitPrice(), item.getTotalPrice());

    itemRepository.saveAndFlush(itemToCreate);

    Optional<CartEntity> cartEntity = getCart(cartId);
    return cartEntity.map(
            entity -> new ResponseEntity<>(getCartConverter().convertEntityToDto(entity), null,
                HttpStatus.CREATED))
        .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));

  }

  @Override
  public ResponseEntity<CartDto> updateItem(final String cartId, final ItemDto item) {
    if (StringUtils.isBlank(item.getId())) {
      return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
    }

    Optional<CartEntity> cart = getCart(cartId);
    if (cart.isEmpty()) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    Optional<ItemEntity> existingItem = itemRepository.findItemByCartIdAndId(cartId, item.getId());
    if (existingItem.isEmpty()) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    if (StringUtils.isBlank(item.getArticleId()) || StringUtils
        .equals(existingItem.get().getArticleId(), item.getArticleId())) {

      if (StringUtils.isNotBlank(item.getName())) {
        existingItem.get().setName(item.getName());
      }

      if (item.getUnitPrice() != null && item.getUnitPrice().compareTo(BigDecimal.ZERO) > 0) {
        existingItem.get().setUnitPrice(item.getUnitPrice());
      }

      if (item.getTotalPrice() != null && item.getTotalPrice().compareTo(BigDecimal.ZERO) > 0) {
        existingItem.get().setTotalPrice(item.getTotalPrice());
      }

      itemRepository.saveAndFlush(existingItem.get());

      updateQuantity(existingItem.get(), cart.get(), item);

      Optional<CartEntity> cartEntity = getCart(cartId);
      return cartEntity.map(
              entity -> new ResponseEntity<>(getCartConverter().convertEntityToDto(entity), null,
                  HttpStatus.OK))
          .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));

    } else {
      return new ResponseEntity<>(HttpStatus.CONFLICT);
    }
  }

  private void updateQuantity(final ItemEntity existingItem, final CartEntity cart,
      final ItemDto item) {
    if (item.getQuantity() != null) {
      if (item.getQuantity().compareTo(BigDecimal.ZERO) > 0) {
        existingItem.setQuantity(item.getQuantity());
        itemRepository.saveAndFlush(existingItem);
      } else {
        removeItem(cart, item.getId());
      }
    } else {
      LOG.warn("No Quantity in item: {}", item);
    }
  }

  @Override
  public ResponseEntity<CartDto> removeItem(final String cartId, final String itemId) {
    Optional<CartEntity> cartBeforeRemoval = getCart(cartId);
    if (cartBeforeRemoval.isEmpty()) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    boolean removed = removeItem(cartBeforeRemoval.get(), itemId);
    if (!removed) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    Optional<CartEntity> cartEntity = getCart(cartId);
    return cartEntity.map(
            entity -> new ResponseEntity<>(getCartConverter().convertEntityToDto(entity), null,
                HttpStatus.OK))
        .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
  }

  private boolean removeItem(final CartEntity cart, final String itemId) {
    Optional<ItemEntity> itemToRemove = cart.getItems().stream()
        .filter(itemEntity -> itemId.equals(itemEntity.getId()))
        .findAny();

    if (itemToRemove.isEmpty()) {
      return false;
    }

    itemRepository.delete(itemToRemove.get());
    cartRepository.refresh(cart);
    return true;
  }

  @Override
  @Transactional
  public boolean deleteCart(final String cartId) {
    return cartRepository.deleteCartById(cartId) > 0;
  }

  @Override
  public Page<CartEntity> findAll(final Pageable pageable) {
    return cartRepository.findAll(pageable);
  }

  @Override
  public ResponseEntity<CartDto> updateCart(final String cartId, final CartDto cartDto,
      final String customerId) {
    final Optional<CartEntity> cart = getCart(cartId);
    if (cart.isEmpty()) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    cart.get().setCustomerId(customerId);

    if (cartDto != null && StringUtils.isNotBlank(cartDto.getCurrencyId())) {
      cart.get().setCurrencyId(cartDto.getCurrencyId());
    }

    cartRepository.saveAndFlush(cart.get());
    return new ResponseEntity<>(getCartConverter().convertEntityToDto(cart.get()), null,
        HttpStatus.OK);
  }

  protected Converter<CartEntity, CartDto> getCartConverter() {
    return cartConverter;
  }
}
