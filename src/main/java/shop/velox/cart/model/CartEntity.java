package shop.velox.cart.model;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.function.Predicate;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import shop.velox.commons.model.AbstractEntity;
import shop.velox.commons.security.annotation.OwnerId;

@Entity
@Table(name = "CARTS")
public class CartEntity extends AbstractEntity {

  private static final long serialVersionUID = 8368633640396558528L;

  @Column(name = "ID", unique = true, nullable = false)
  private final String id;
  @OneToMany(fetch = FetchType.LAZY, mappedBy = "cart", cascade = CascadeType.REMOVE)
  @NotNull
  private final Set<ItemEntity> items;
  @Column(name = "DIRTY")
  private boolean dirty = false;
  @Column(name = "CUSTOMER_ID")
  private String customerId;
  @Column(name = "CURRENCY_ID")
  @NotNull
  private String currencyId;

  public CartEntity() {
    this(UUID.randomUUID().toString());
  }

  public CartEntity(final String id) {
    this(id, "");
  }

  public CartEntity(final String id, String customerId) {
    this.id = id;
    items = new HashSet<>();
    this.customerId = customerId;
  }

  public boolean addItem(final ItemEntity item) {
    return this.items.add(item);
  }

  public boolean isDirty() {
    return dirty;
  }

  public void setDirty(final boolean dirty) {
    this.dirty = dirty;
  }

  @OwnerId
  public String getCustomerId() {
    return customerId;
  }

  public void setCustomerId(final String customerId) {
    this.customerId = customerId;
  }

  public String getCurrencyId() {
    return currencyId;
  }

  public void setCurrencyId(String currencyId) {
    this.currencyId = currencyId;
  }

  public String getId() {
    return id;
  }

  public @NotNull Set<ItemEntity> getItems() {
    return items;
  }

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
  }

  public static Predicate<CartEntity> anonymousCart = cartEntity -> Optional.ofNullable(cartEntity)
      .map(e -> e.getCustomerId() == null || e.getCustomerId().isEmpty()).orElse(false);
}
